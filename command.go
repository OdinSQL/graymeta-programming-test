// Benjamin Knigge
package main

import "strings"

// I decided to create an enum for the possible commands
type Command uint8

const (
	DEPEND Command = iota
	INSTALL
	REMOVE
	LIST
	END
	InvalidCommand
)

// if you have an enum but want the string representation
func (c Command) String() string {
	commands := []string{"DEPEND", "INSTALL", "REMOVE", "LIST", "END", "INVALID_COMMAND"}
	return commands[uint8(c)]
}

// when you have a string and want the command
func CommandFromString(s string) Command {
	// The instructions do not explicitly state that end will be upper case
	// This was likely an oversight if that is the case remove this if statement
	if strings.ToUpper(s) == "END" {
		return END
	}
	switch s {
	case "DEPEND":
		return DEPEND
	case "INSTALL":
		return INSTALL
	case "REMOVE":
		return REMOVE
	case "LIST":
		return LIST
	case "END":
		return END
	default:
		return InvalidCommand
	}
}
