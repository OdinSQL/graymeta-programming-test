// Benjamin Knigge
package main

func (app *App) feedTheAppInput() {
	app.parseLine("DEPEND TELNET TCPIP NETCARD")
	app.parseLine("DEPEND TCPIP NETCARD")
	app.parseLine("DEPEND DNS TCPIP NETCARD")
	app.parseLine("DEPEND BROWSER TCPIP HTML")
	app.parseLine("INSTALL NETCARD")
	app.parseLine("INSTALL TELNET")
	app.parseLine("INSTALL foo")
	app.parseLine("REMOVE NETCARD")
	app.parseLine("INSTALL BROWSER")
	app.parseLine("INSTALL DNS")
	app.parseLine("LIST")
	app.parseLine("REMOVE TELNET")
	app.parseLine("REMOVE NETCARD")
	app.parseLine("REMOVE DNS")
	app.parseLine("REMOVE NETCARD")
	app.parseLine("INSTALL NETCARD")
	app.parseLine("REMOVE TCPIP")
	app.parseLine("REMOVE BROWSER")
	app.parseLine("REMOVE TCPIP")
	app.parseLine("LIST")
	app.parseLine("END")
}
