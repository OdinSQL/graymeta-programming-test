// Benjamin Knigge
package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// I create a struct instead of dealing with global variables
type App struct {
	shouldExit bool
	/*
		I had tried working with https://github.com/gonum/gonum and found it to be very unintuitive
		After much frustration I decided just to implement my own solution
	*/
	dependencyGraph Graph
	installedGraph  Graph
}

// parse the input and call the function for the appropriate command
func (a *App) parseLine(input string) {
	fmt.Println(input)
	input = strings.TrimSpace(input)
	// input string should not be over 80 characters in length
	if len(input) > 80 {
		a.invalidInputLength()
		return
	}
	parts := strings.Fields(input)
	if len(parts) > 0 {
		c := CommandFromString(parts[0])
		switch c {
		case DEPEND:
			if len(parts) > 1 {
				// part 0 is the command, part 1 is the application, parts 2 or more are dependencies
				a.depend(parts[1], parts[2:]...)
			} else {
				// If there isn't an application it's an error
				a.invalidArguments(c)
			}

		case INSTALL:
			// need to be at least 2 part for an install the command and the application to install
			if len(parts) > 1 {
				a.install(parts[1])
			} else {
				a.invalidArguments(c)
			}
		case REMOVE:
			// need to be at least 2 part for a remove the command and the application to remove
			if len(parts) > 1 {
				a.remove(parts[1])
			} else {
				a.invalidArguments(c)
			}
		case LIST:
			a.list()
		case END:
			a.end()
		case InvalidCommand:
			a.invalidCommand(parts[0])
		}
	} else {
		a.invalidCommand(input)
	}
}

func (a *App) addDependencyNodeIfNeeded(name string, dependencies []string) (added bool, node *Node) {
	var depNodes []*Node

	for _, dep := range dependencies {
		_, dn := a.addDependencyNodeIfNeeded(dep, nil)
		depNodes = append(depNodes, dn)
	}

	if a.dependencyGraph[name] == nil {
		n := &Node{name, depNodes, false}
		a.dependencyGraph[name] = n
		return true, n
	} else {
		if a.dependencyGraph[name].Dependencies == nil {
			a.dependencyGraph[name].Dependencies = depNodes
		}
		return false, a.dependencyGraph[name]
	}
}

func (a *App) getNodesToInstall(name string) []*Node {
	installNode := a.dependencyGraph[name]
	if installNode == nil {
		// there are no dependencies create a new node
		newNode := &Node{Name: name, Dependencies: nil}
		return []*Node{newNode}
	} else {
		toInstall := []*Node{installNode}
		if installNode.Dependencies == nil {
			return toInstall
		} else {
			for _, n := range installNode.Dependencies {
				if a.installedGraph[n.Name] == nil {
					toInstall = append(toInstall, a.getNodesToInstall(n.Name)...)
				}
			}
			return toInstall
		}
	}
}

// create a function for each command

// add dependencies to a map and print them
func (a *App) depend(appName string, args ...string) {
	// a.depMap[appName] = args
	a.addDependencyNodeIfNeeded(appName, args)

}

// install an app and it's needed dependencies
func (a *App) install(appName string) {
	if a.installedGraph[appName] != nil {
		fmt.Printf("%s is already installed\n", appName)
	} else {
		nodes := a.getNodesToInstall(appName)
		for i := len(nodes) - 1; i >= 0; i-- {
			// ensure that you're only installing each dependency once
			if a.installedGraph[nodes[i].Name] == nil {
				fmt.Printf("%s successfully installed\n", nodes[i].Name)
				a.installedGraph[nodes[i].Name] = nodes[i]
			}

		}
	}
	a.installedGraph[appName].ManuallyInstalled = true
}

func (a *App) isStillNeeded(node *Node) bool {

	for _, n := range a.installedGraph {
		for _, deps := range n.Dependencies {
			if node.Name == deps.Name {
				return true
			}
		}
	}
	return false
}

// remove an app and it's unused dependencies
func (a *App) remove(appName string) {
	// check the map and remove unneeded dependencies and then the app
	node := a.installedGraph[appName]
	if node == nil {
		fmt.Printf("%s is not installed\n", appName)
		return
	}
	if a.isStillNeeded(node) {
		fmt.Printf("%s is still needed\n", appName)
		return
	} else {
		delete(a.installedGraph, appName)
		fmt.Printf("%s successfully removed\n", appName)
		// need to check dependencies recursively
		for _, n := range node.Dependencies {
			// don't remove a dependency that was manually installed
			if !a.isStillNeeded(n) && !n.ManuallyInstalled {
				fmt.Printf("%s is no longer needed\n", n.Name)
				a.remove(n.Name)
			}
		}
	}
}

// list installed apps
func (a *App) list() {
	for _, node := range a.installedGraph {
		fmt.Printf("%s\n", node.Name)
	}
}

// exit the program
func (a *App) end() {
	a.shouldExit = true
}

// if an invalid command is entered print an error
func (a *App) invalidCommand(input string) {
	fmt.Printf("'%s' is not a valid command string.\n", input)
}

// if an invalid line of text was entered
func (a *App) invalidInputLength() {
	fmt.Printf("Invalid input length. 80 char per line max.")
}

// if a command should have arguments but doesn't
func (a *App) invalidArguments(c Command) {
	fmt.Printf("Invalid arguments for the command '%s'.", c.String())
}

// read the lines one at a time
func (a *App) scanLines() {
	scanner := bufio.NewScanner(os.Stdin)
	// It's nice to have some indication that the application is waiting f or input
	//fmt.Print("Enter a command: ")
	for scanner.Scan() {
		a.parseLine(scanner.Text())
		if a.shouldExit {
			break
		}
	}
}
