// Benjamin Knigge
package main

import "testing"

/*
This is a garbage test but I was tired and wanted to send this to GrayMeta
Before my interview in the morning so that we would have something to talk about.
*/

func TestCommand(t *testing.T) {
	app := App{shouldExit: false,
		dependencyGraph: Graph{},
		installedGraph:  Graph{},
	}

	app.feedTheAppInput()
}
