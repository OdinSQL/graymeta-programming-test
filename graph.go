// Benjamin Knigge
package main

type Graph map[string]*Node

type Node struct {
	Name              string
	Dependencies      []*Node
	ManuallyInstalled bool
}
