// Benjamin Knigge
package main

func main() {
	//create the app
	app := App{shouldExit: false,
		dependencyGraph: Graph{},
		installedGraph:  Graph{},
	}
	/*
		debug := true
		if !debug {
	*/
	// scan the lines
	app.scanLines()
	/*
		} else {
			// Go's/Goland's debugger, delve does not work when reading from stdin
			// This allows me to run the app with the debugger working
			app.feedTheAppInput()
		}
	*/

}
